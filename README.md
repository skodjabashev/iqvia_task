# IQVIA_task

Contact management asynchronous API with Flask.

There is full swagger doc on http://localhost:5000/
![Alt text](swagger.jpg "Swagger Doc")
_____
To Start Celery you need to execute:
celery start - celery -A start.celery worker --loglevel=info --pool=solo

_____
Since there is no business logic I created integration test ran though unit. For the tests to pass both the Flask and 
Celery app must be running.