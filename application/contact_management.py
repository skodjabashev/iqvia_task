import logging
import json
import random
import string

import celery
from datetime import timedelta
from sqlalchemy import exc
from application.models.contact import Contact as ContactModel
from application.models.contact import Email as EmailModel
from application.database import db_session

logger = logging.getLogger(__name__)

ALREADY_EXISTS_DB_ERROR = 'gkpj'


class ContactManagement:

    def get_contacts(self):
        contacts = db_session.query(ContactModel)\
            .join(EmailModel, EmailModel.contact_id == ContactModel.id, isouter=True).all()
        return [c.to_json(c.children) for c in contacts]

    def create_contact(self, data):
        self.celery_create_contact.delay(data)
        return "Async creation of contact"

    @celery.task(bind=True, name='celery_create')
    def celery_create_contact(self, data):
        post_data = json.loads(data)
        new_user = ContactModel(username=post_data['username'],
                                first_name=post_data['first_name'],
                                last_name=post_data['last_name'])
        db_session.add(new_user)
        try:
            db_session.commit()
        except exc.SQLAlchemyError as ex:
            if ex.code == ALREADY_EXISTS_DB_ERROR:
                db_session.remove()
                return 'Conflict', 409
            else:
                db_session.remove()
                return 'Internal Server Error', 500

        db_session.remove()
        return 'Created', 201

    def delete_contact(self, data):
        self.celery_delete_contact.delay(data)
        return "Async deletion of contact"

    @celery.task(bind=True, name='celery_delete')
    def celery_delete_contact(self, data):
        post_data = json.loads(data)
        contact = db_session.query(ContactModel).filter(ContactModel.username == post_data['username']).all()
        if not contact:
            return 'Not Found', 404
        db_session.delete(contact[0])
        try:
            db_session.commit()
        except exc.SQLAlchemyError as ex:
            db_session.remove()
            return 'Internal Server Error', 500
        db_session.remove()
        return 'No Content', 204

    def edit_contact(self, username, data):
        self.celery_edit_contact.delay(username, data)
        return "Async editing of contact"

    @celery.task(bind=True, name='celery_update')
    def celery_edit_contact(self, username, data):
        post_data = json.loads(data)
        contact = db_session.query(ContactModel).filter(ContactModel.username == username).all()
        if not contact:
            return 'Not Found', 404
        contact = contact[0]

        if 'username' in post_data:
            contact.username = post_data['username']
        if 'first_name' in post_data:
            contact.first_name = post_data['first_name']
        if 'last_name' in post_data:
            contact.last_name = post_data['last_name']

        try:
            db_session.commit()
        except exc.SQLAlchemyError as ex:
            if ex.code == ALREADY_EXISTS_DB_ERROR:
                db_session.remove()
                return f'Username {post_data["username"]} already exists', 409
            db_session.remove()
            return 'Internal Server Error', 500

        for em in post_data['emails']:
            email = EmailModel(email=em, contact_id=contact.id)
            db_session.add(email)
            try:
                db_session.commit()
            except:
                pass
            db_session.remove()
        return 'Ok', 200

    @celery.task.periodic_task(bind=True, run_every=timedelta(seconds=15))
    def periodically_create_user(self):
        letters = string.ascii_lowercase
        sample = {
            'username': ''.join(random.choice(letters) for i in range(10)),
            'first_name': ''.join(random.choice(letters) for i in range(10)),
            'last_name': ''.join(random.choice(letters) for i in range(10))
        }
        self.create_contact(data=sample)

