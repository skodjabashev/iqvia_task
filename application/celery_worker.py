import random
import string
from datetime import timedelta

from application.contact_management import ContactManagement
from start import celery


@celery.task.periodic_task(run_every=timedelta(seconds=15))
def periodically_create_user():
    letters = string.ascii_lowercase
    sample = {
        'username': ''.join(random.choice(letters) for i in range(10)),
        'first_name': ''.join(random.choice(letters) for i in range(10)),
        'last_name': ''.join(random.choice(letters) for i in range(10))
    }
    ContactManagement.create_contact(data=sample)