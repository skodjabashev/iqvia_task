from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from application.database import Base


class Contact(Base):
    __tablename__ = 'contacts'

    id = Column(Integer, primary_key=True)
    username = Column(String(32), unique=True)
    first_name = Column(String(32), unique=False)
    last_name = Column(String(32), unique=False)

    def __init__(self, username: str, first_name: str, last_name: str):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name

    def to_json(self, emails):
        return {'username': self.username,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'emails': [e.email for e in emails]}


class Email(Base):
    __tablename__ = 'emails'

    id = Column(Integer, primary_key=True)
    email = Column(String(64), unique=True)
    contact_id = Column(Integer, ForeignKey('contacts.id'))
    parent = relationship(Contact, backref=backref("children", cascade="all,delete"))
