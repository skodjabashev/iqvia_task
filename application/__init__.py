import logging
from flask import Flask
from apis import api
from application.database import init_db
from application.models.contact import Base, Contact
from application.celery_app import make_celery


def create_app():
    """Construct the core application."""
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.sqlite'
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
    app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
    app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

    init_db()
    api.init_app(app)
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)

    return app
