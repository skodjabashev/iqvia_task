import json
import unittest
import requests



class TestContactManagement(unittest.TestCase):
    url = 'http://localhost:5000/v1/Contact%20Management/contacts'

    def test_integration_test_api(self):
        curr_numb_of_contacts = len(self.get_contacts())

        ### Create Contact
        data = {'username': 'testname1',
                'first_name': 'first_name',
                'last_name': 'last_name'}
        request_response = requests.post(self.url,
                                         headers={'Content-type': 'application/json', 'Accept': 'application/json'},
                                         data=json.dumps(data))
        self.assertEqual(curr_numb_of_contacts+1, len(self.get_contacts()))

        ### Edit Contact
        data = {'first_name': 'Stoyko',
                'last_name': 'Kodzhabashev',
                'emails': ['testemail@gmail.com']}
        request_response = requests.put(self.url + '/testname1',
                                         headers={'Content-type': 'application/json', 'Accept': 'application/json'},
                                         data=json.dumps(data))
        data = self.get_contacts()
        for d in data:
            if d['username'] == 'testname1':
                self.assertEqual(d['first_name'], 'Stoyko')
                self.assertEqual(d['last_name'], 'Kodzhabashev')
                self.assertEqual(1, len(d['emails']))

        ### Add Additional email
        data = {'emails': ['testemail2@gmail.com']}
        request_response = requests.put(self.url + '/testname1',
                                        headers={'Content-type': 'application/json', 'Accept': 'application/json'},
                                        data=json.dumps(data))
        data = self.get_contacts()
        for d in data:
            if d['username'] == 'testname1':
                self.assertEqual(2, len(d['emails']))


        ### Delete Contact
        data = {'username': 'testname1'}
        request_response = requests.delete(self.url,
                                         headers={'Content-type': 'application/json', 'Accept': 'application/json'},
                                         data=json.dumps(data))
        self.assertEqual(curr_numb_of_contacts, len(self.get_contacts()))

    def get_contacts(self):
        request_response = requests.get(self.url, headers={'Content-type': 'application/json', 'Accept': 'application/json'})
        # if request_response.status_code == 200:
        return json.loads(request_response.text)
