from flask_restplus import Api

from .contact_management import api as contact_management

api = Api(
    title='IQVIA_task',
    version='1.0.0',
    description='This service contact management API',
    prefix='/v1'
)

api.namespaces.clear()
api.add_namespace(contact_management)
