import logging
from flask_restplus import Namespace, Resource, fields
from flask import jsonify, request
from flask_sqlalchemy import SQLAlchemy
from application.contact_management import ContactManagement

logger = logging.getLogger(__name__)

api = Namespace('Contact Management', description='Allows contacts to be managed by creating new ones, deleting or '
                                                  'updating existing contacts')
db = SQLAlchemy()
ALREADY_EXISTS_DB_ERROR = 'gkpj'
CM = ContactManagement()

@api.route('/contacts')
class Contacts(Resource):
    create_user_payload = api.model('PostData', {
        'username': fields.String(required=True),
        'first_name': fields.String(required=True),
        'last_name': fields.String(required=True)
    })

    delete_user_payload = api.model('PostData', {
        'username': fields.String(required=True)
    })

    @api.doc('Get list of all contacts')
    def get(self):
        return jsonify(CM.get_contacts())

    @api.doc('Create a new contact')
    @api.expect(create_user_payload, validate=True)
    def post(self):
        return CM.create_contact(request.data.decode())

    @api.doc('Delete an existing contact')
    @api.expect(delete_user_payload, validate=True)
    def delete(self):
        return CM.delete_contact(request.data.decode())


@api.route('/contacts/<username>')
@api.param(name='username', description='The username of the contact you are updating')
class Contact(Resource):
    update_user_payload = api.model('PostData', {
        'username': fields.String(),
        'first_name': fields.String(),
        'last_name': fields.String(),
        'emails': fields.List(fields.String())
    })

    @api.doc('Update an existing contact')
    @api.expect(update_user_payload)
    def put(self, username):
        return CM.edit_contact(username, request.data.decode())

