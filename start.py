from application import create_app
from application.celery_app import make_celery

app = create_app()
celery = make_celery(app)

if __name__ == "__main__":
    app.run()